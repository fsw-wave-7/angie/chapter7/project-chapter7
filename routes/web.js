var express = require("express");
var web = express.Router();
const HomeController = require("../controllers/web/HomeController");
const AuthController = require("../controllers/web/AuthController");
const homeController = new HomeController();
const authController = new AuthController();
const restrict = require("../middlewares/restrict");
const passport = require("../lib/passport");
const bodyParser = require("body-parser");
web.use(bodyParser.json());

// Authentication
web.get("/login", authController.login);
web.post("/login", authController.doLogin);

// restrict
web.use(restrict);

/* GET home page. */
web.get("/", homeController.dashboard); //home dashboard
web.get("/admin", homeController.listAdmin); //list admin
web.get("/editAdmin/:id", homeController.editAdmin); //edit admin
web.post("/editAdmin/:id", homeController.saveAdmin); //edit admin
web.get("/delete/:id", homeController.deleteAdmin); //delete admin
web.get("/posts", homeController.tampilPost); //list post

// register admin
web.get("/register", homeController.addAdmin); //add admin
web.post("/register", authController.register);
web.get("/whoami", authController.whoami);
web.get("/logout", authController.logout); //clear sesi

// user
web.get("/listUser", homeController.getUser);

module.exports = web;
