var express = require("express");
var api = express.Router();
const UserController = require("../controllers/api/UserController");
const AuthController = require("../controllers/api/AuthController");
const userController = new UserController();
const authController = new AuthController();
const bodyParser = require('body-parser')

api.use(bodyParser.json())

// Authentication
api.post("/login", authController.login);
api.post("/register", authController.register);

/* GET home page user. */
//home user berisi daftar postingan user
api.get("/", userController.getUser);
api.get("/:id", userController.getDetailUser);

//halaman posting user
api.get("/post", (req, res) => {
  res.send("halaman user untuk posting");
});

//route untuk posting
api.post("/post", userController.insertPost);
api.post("/post/:id", userController.updatePost);

// route untuk menampilkan seluruh postingan user
api.get("/posts", (req, res) => {
  res.send("daftar seluruh postingan user");
});

//delete post
api.delete("/post/:id", userController.deletePost);

module.exports = api;
