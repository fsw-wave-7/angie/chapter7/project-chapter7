const { User, History } = require('../../models');

class UserController {
  getUser = (req, res) => {
    User.findAll().then((user) => {
      res.send(user);
    });
  };

  getDetailUser = (req, res) => {
    User.findOne({
      where: { id: req.params.id },
      include: [
        {
          model: History,
          as: 'history',
        },
      ],
    }).then((user) => {
      res.send(user);
    });
  };

  insertPost = (req, res) => {
    History.create({
      post: req.body.post,
      user_id: req.body.user_id,
    }).then(res.send('berhasil memposting'));
  };

  updatePost = (req, res) => {
    History.update(
      {
        post: req.body.post,
      },
      {
        where: { id: req.params.id },
      }
    ).then(res.send('berhasil update'));
  };

  deletePost = (req, res) => {
    History.destroy({
      where: {
        id: req.params.id,
      },
    }).then(() => {
      res.send('berhasil menghapus');
    });
  };
}

module.exports = UserController;
