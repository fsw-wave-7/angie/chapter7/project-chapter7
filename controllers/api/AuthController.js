const { User } = require("../../models");
const bcrypt = require("bcrypt")
const passport = require("../../lib/passport-jwt");

function format(user) {
  const { id, username } = user 
  return {id,
  username,
  accessToken: user.generateToken()
  } 
}
class AuthController {
  register = async (req, res) => {
    User.register({
      username: req.body.username,
      password: req.body.password
    })
      .then(() => {
        res.send("data berhasil disimpan");
      })
      .catch((err) => res.send(err.message));
  };

  login = (req, res) => {
    User.authenticate(req.body)
      .then((user) => {
        res.json(format(user)) 
      })
      .catch((err) => res.send(err));
  };

  whoami = (req, res) => {
    res.render('index', req.user.dataValues)
  };

  logout = (req, res) => {
    res.clearCookie('loginData')
    res.redirect('/')
  }  
}

module.exports = AuthController;
